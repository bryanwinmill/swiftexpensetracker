//
//  ExpenseController.swift
//  SwiftExpenseTracker
//
//  Created by Bryan Winmill on 10/6/17.
//  Copyright © 2017 Bryan Winmill. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class ExpenseController {
    
    static let sharedInstance = ExpenseController()
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    func createExpense(place: String, amount: Double, date: String, notes: String) -> Payment? {
        
        guard let payment = Payment(place: place, amount: amount, date: date, notes: notes, context: context) else { return nil }
        
        save()
        
        return payment
    }
    
    func fetchExpenses() -> [Payment] {
        //Create a request for the context.
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Payment")
        
        //Create an array of Payment objects that we get back from our fetch
        var tasks: [Payment] = []
        
        do {
            //Fetching the Payments - Ensure compiler knows types
            tasks = try context.fetch(request) as! [Payment]
        } catch {
            print(error.localizedDescription)
        }
        
        return tasks
    }
    
    func edit(payment: Payment, place: String, amount: Double, date: String, notes: String) {
        payment.name = place
        payment.amount = amount
        payment.theDate = date
        payment.notes = notes
        
        save()
    }
    
    func delete(payment: Payment) {
        payment.managedObjectContext?.delete(payment)
        save()
    }
    
    private func save() {
        do {
            try context.save()
        } catch let error {
            print(error.localizedDescription)
        }
    }
}
