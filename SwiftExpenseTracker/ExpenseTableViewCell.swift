//
//  ExpenseTableViewCell.swift
//  SwiftExpenseTracker
//
//  Created by Bryan Winmill on 10/6/17.
//  Copyright © 2017 Bryan Winmill. All rights reserved.
//

import UIKit

class ExpenseTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
