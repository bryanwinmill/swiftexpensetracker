//
//  Payment+CoreDataClass.swift
//  SwiftExpenseTracker
//
//  Created by Bryan Winmill on 10/6/17.
//  Copyright © 2017 Bryan Winmill. All rights reserved.
//
//

import Foundation
import CoreData

@objc(Payment)
public class Payment: NSManagedObject {

    
    convenience init?(place: String, amount: Double, date: String, notes: String?, context: NSManagedObjectContext) {
        
        guard let entity = NSEntityDescription.entity(forEntityName: "Payment", in: context) else {return nil}
        
        self.init(entity: entity, insertInto: context)
    
        self.name = place
        self.amount = amount
        self.theDate = date
    
    }
}
