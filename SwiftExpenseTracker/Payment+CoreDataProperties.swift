//
//  Payment+CoreDataProperties.swift
//  SwiftExpenseTracker
//
//  Created by Bryan Winmill on 10/6/17.
//  Copyright © 2017 Bryan Winmill. All rights reserved.
//
//

import Foundation
import CoreData


extension Payment {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Payment> {
        return NSFetchRequest<Payment>(entityName: "Payment")
    }

    @NSManaged public var amount: Double
    @NSManaged public var name: String
    @NSManaged public var notes: String?
    @NSManaged public var theDate: String

}
