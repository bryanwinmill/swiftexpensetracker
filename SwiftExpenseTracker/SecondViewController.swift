//
//  SecondViewController.swift
//  SwiftExpenseTracker
//
//  Created by Bryan Winmill on 10/3/17.
//  Copyright © 2017 Bryan Winmill. All rights reserved.
//

import UIKit



class SecondViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate {
    
    var task: Payment?
    
    @IBOutlet var deleteButton: UIButton!
    
    @IBAction func deleteEntry(_ sender: UIButton) {
        
        if let task = self.task {
            ExpenseController.sharedInstance.delete(payment: task)
            navigationController?.popViewController(animated: true)
        }
        
    }
    
    @IBOutlet var editPlace: UITextField!
    @IBOutlet var editCost: UITextField!
    @IBOutlet var editDate: UITextField!
    @IBOutlet var editNotes: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let task = self.task {
            self.editCost.text = "\(task.amount)"
            self.editDate.text = task.theDate
            self.editPlace.text = task.name
            self.editNotes.text = task.notes
            
            self.deleteButton.isHidden = false
        }
        
        self.editPlace.delegate = self
        self.editCost.delegate = self
        self.editDate.delegate = self
        self.editNotes.delegate = self
        
        
        let saveButton = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(buttonTapped(_:)))
        
        /*let deleteButton = UIBarButtonItem(barButtonSystemItem: ., target: self, action: #selector(buttonTapped(_:)))*/
        
        self.navigationItem.rightBarButtonItems = [saveButton]
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        
        return true
    }
    

    @objc func buttonTapped (_ sender : UIButton) {
        
        let place = editPlace.text ?? "Not Entered"
        let cost = Double(editCost.text ?? "0") ?? 0
        let theDate = editDate.text ?? "Not Entered"
        let notes = editNotes.text ?? "Not Entered"
        
        if let task = self.task {
            ExpenseController.sharedInstance.edit(payment: task, place: place, amount: cost, date: theDate, notes: notes)
        } else {
            _ = ExpenseController.sharedInstance.createExpense(place: place, amount: cost, date: theDate, notes: notes)
        }
        
        self.navigationController?.popViewController(animated: true)
    }
    
    

}
